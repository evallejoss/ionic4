import { Component, OnInit, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { HomeService } from '../../services/home.service';
import { RootObject } from '../../interfaces/interfaceContent';
import { AuthService } from '../../services/auth.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChild(IonInfiniteScroll) infinitiScroll : IonInfiniteScroll;

  feeds : RootObject;

  constructor( private homeService : HomeService, private auth : AuthService ) { }

  ngOnInit() {
      
    //  this.homeService.getContent(20120628161057, 3).subscribe(
    //     data => {
    //       this.feeds = data
    //     },
    //     error =>{
    //         console.log(error)
    //     }
    //  )

    this.auth.getAppToken();
  }

  // llamar a funcion refresh  datos en dom 
  // doRefresh( event ){

  //   setTimeout(() => {
  //     this.data = Array(40);
  //     event.target.complete();
  //   }, 1500);

  // }

  

  // loadData(event){
  //   this.homeService.getContent().subscribe(
  //     data =>{
  //       if(this.feeds.length > 50){
  //         event.target.complete();
  //         this.infinitiScroll.disabled = true;
  //         return;
  //       }
  //       this.feeds.push(data)
  //       event.target.complete();
  //     },
  //     error =>{
  //       console.log(error);
  //     }
  //   )
    
  // }

}
