import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { SideMenu } from '../../interfaces/interfaceContent';
import { Observable } from 'rxjs';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  selectedPath = '';
  component : Observable<SideMenu []>;

  constructor( private dataService : DataService, private router : Router) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
   }

  ngOnInit() {
    this.component = this.dataService.getMenuOpts();

    console.log(this.component)
  }

}
