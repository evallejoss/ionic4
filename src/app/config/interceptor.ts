import {HTTP_INTERCEPTORS} from '@angular/common/http';

import { InterceptorTokenService } from '../interceptors/interceptor-token.service';
import { InterceptorCryptoService } from '../interceptors/interceptor-crypto.service';


export const interceptorProviders = 
   [
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorCryptoService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorTokenService, multi: true }  
];