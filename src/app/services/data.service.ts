import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { SideMenu } from '../interfaces/interfaceContent';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http : HttpClient) { }

  getMenuOpts(){
    return this.http.get<SideMenu []>('/assets/data/menu.json')
  }

}
