import { Injectable } from "@angular/core";
import { HttpClient, HttpRequest } from "@angular/common/http";
import { Storage } from "@ionic/storage";
import * as moment from "moment";
import { Observable, from } from "rxjs";
import { map, delay } from "rxjs/operators";
import { Config } from '../config/config';

@Injectable({
  providedIn: "root"
})
export class TokenService {
  timeToken: number = Config.TOKEN_TIME; // tiempo del token consultado
  
  constructor(private storage: Storage, private http: HttpClient) {}

  setTokenStorage(dataToken) {
    return this.storage.set("JWT_TOKEN", dataToken);
  }

  async getTokenStorage() {
    await this.storage.get("JWT_TOKEN");
  }

  addToken(request: HttpRequest<any>, token: any) {
    if (token) {
      let clone: HttpRequest<any>;
      clone = request.clone({
        setHeaders: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: `Normal ${token}`
        }
      });
      return clone;
    }
    return request;
  }

  getToken(): Observable<string> {
    return from(this.storage.get("JWT_TOKEN")).pipe(
      map(data => {
        let todayToken = moment().format("YYYY-MM-DD HH:mm:ss");
        let expiration = moment(todayToken).isAfter(data.expiration);
        if (!expiration) {
          return data.token;
        }
      })
    );
  }

  refresh(): Observable<any> {
    return this.http.get("/assets/data/token.json").pipe(
      delay(5000),
      map((data: any) => {
        let expirationToken = moment()
          .add(5, "minutes")
          .format("YYYY-MM-DD HH:mm:ss");
        let dataToken  = {
          expiration: expirationToken,
          token: data.token
        };
        this.storage.remove("JWT_TOKEN");
        this.setTokenStorage(dataToken);
        return data.token;
      })
    );
  }
}
