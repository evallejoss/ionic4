import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor( private http : HttpClient) { }

  serverTime(){
    return this.http.get(`${Config.URL}Server/Time`)
  }
}
