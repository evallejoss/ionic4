import { Injectable } from '@angular/core';
import { HelpersService } from './helpers.service';
import * as jwt from 'jsonwebtoken';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // header : any = {alg:'HS256',typ:'JWT'};
  // algorithm : string = "HS256";
  appSecret : string = "!QAZzaq12wsx";
  payload : any = {
      iss: "CM3.0APP",
      sub : "AppToken",
      app_name : 'CanalMobile',
      app_id: '686C157C-5613-4E7F-8943-45964002F888',
      aud : "CMAPI",
      nbf : '',
      iat: '',
      exp : '',
  }

  constructor( private helperService : HelpersService) { }

  getAppToken(){
    this.helperService.serverTime()
        .subscribe( (time : any) =>{
          const date = new Date(time.utc).getTime()/1000;
          this.payload.nbf = date - 1;
          this.payload.iat = date; 
          this.payload.exp = (date + 120);
          let JWT = jwt.sign(this.payload, this.appSecret)
          console.log(JWT)
          return JWT;
          //token de retorno dura 15 min
          //status 401 si falla el tokenapp
        })
  }
}
