import { Injectable } from '@angular/core';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import * as CryptoJS from 'crypto-js'
import { Config } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class EncrypDecryptService {

  constructor() { }

  set(value){
    const key = CryptoJS.enc.Utf8.parse(Config.KEY);
    const iv = CryptoJS.enc.Utf8.parse(Config.IV);
    const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(JSON.stringify(value)), key,
    {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    return encrypted.toString();
  }

  get(value){
    const key = CryptoJS.enc.Utf8.parse(Config.KEY);
    const iv = CryptoJS.enc.Utf8.parse(Config.IV);
    const decrypted = CryptoJS.AES.decrypt(value, key, {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });
    
    return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
  }

}
