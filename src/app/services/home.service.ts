import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Config } from '../config/config';
import { RootObject } from '../interfaces/interfaceContent';


@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http : HttpClient) { }
  
  queryExecute<T>( query : string, params : string){

     return this.http.get<T>(`${Config.URL}${query}?q=${params}`)
  }

  getContent(seccion : number, lenght : number, orderby?:number, fechaPublicacion?:string){

    const query : string = JSON.stringify({"Include":["Likes","Comentarios{1}^Fecha^"], "Page":{"length": lenght},"OrderBy":"FechaPublicacion"})

    return this.queryExecute<RootObject>(`Secciones/${seccion}/Contenidos`, query);
  }

}
