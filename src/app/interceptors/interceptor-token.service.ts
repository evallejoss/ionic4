import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { Observable, from, throwError, of } from 'rxjs';
import { catchError, mergeMap, switchMap} from 'rxjs/operators';

import { TokenService } from '../services/token.service';

@Injectable({
  providedIn: 'root'
})
export class InterceptorTokenService implements HttpInterceptor{

  constructor(private controlToken : TokenService) { }

  intercept( request : HttpRequest <any>, next : HttpHandler): Observable<HttpEvent<any>>{

      let promise = from(this.controlToken.getToken())

      return from(promise).pipe(mergeMap((token: any) =>{
        const cloneRequest = this.controlToken.addToken(request, token);
        return next.handle(cloneRequest)
      }),
        catchError(error =>{
          if(error.status === 401){
              return this.refreshToken(request, next);
          }
        })
      );
      

  }
  
  refreshToken(request : HttpRequest <any>, next : HttpHandler){

    return this.controlToken.refresh().pipe(
      mergeMap((token : any) =>{
            const cloneRequest = this.controlToken.addToken(request, token);
            return next.handle(cloneRequest)
      })
    )

  }

  
    
}

 



