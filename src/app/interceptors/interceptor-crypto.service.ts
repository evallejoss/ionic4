import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Config } from '../config/config';
import { EncrypDecryptService } from '../services/encryp-decrypt.service';


@Injectable({
  providedIn: 'root'
})
export class InterceptorCryptoService implements HttpInterceptor {

  constructor(private encrypDecrypt : EncrypDecryptService) { }


  intercept( request : HttpRequest <any>, next : HttpHandler): Observable<HttpEvent<any>>{
   
            if(request.method !== 'GET'){
                //Encryptar datos del body
                let encrypt = this.encrypDecrypt.set(request.body);
                // Sobre escribo la llave body del request
                request = request.clone({body: encrypt});
            }
            
            return next.handle(request).pipe(
                map((event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        // console.log('body >>>', event.body)
                        let encrypt = this.encrypDecrypt.set(event.body);
                        // console.log('encrypt data >>>', encrypt)
                        let decrypt = this.encrypDecrypt.get(encrypt);
                        event = event.clone({body: decrypt});
                        // console.log('decrypt data >>>', event)
                    }
                    return event;
                }),
                catchError((error: HttpErrorResponse) => {
                    let data = {};
                    data = {
                        message: error.message,
                        reason: error.status
                    };
                    return throwError(error);
                })
                );
            }
}


